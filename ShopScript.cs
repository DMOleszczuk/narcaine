﻿using UnityEngine;
using UnityEngine.UI;

namespace NarCaine
{
    public class ShopScript : MonoBehaviour
    {
        static int drug1price;
        static int drug2price;
        static int drug3price;

        static int drug1count;
        static int drug2count;
        static int drug3count;

        Text drug1invtxt;
        Button drug1buyone;
        Button drug1buyten;
        Button drug1sellone;
        Button drug1sellten;

        Text drug2invtxt;
        Button drug2buyone;
        Button drug2buyten;
        Button drug2sellone;
        Button drug2sellten;

        Text drug3invtxt;
        Button drug3buyone;
        Button drug3buyten;
        Button drug3sellone;
        Button drug3sellten;

        public static bool enteredshop = false;

        // Use this for initialization
        void Awake()
        {
            if (!enteredshop)
            {
                GetDrugPrice();
            }
            
            drug1invtxt = GameObject.Find("Drug1InvText").GetComponent<Text>();
            drug1buyone = GameObject.Find("Drug1BuyOneButton").GetComponent<Button>();
            drug1buyone.onClick.AddListener(() => BuySellDrugs(1, 1));
            drug1buyten = GameObject.Find("Drug1BuyTenButton").GetComponent<Button>();
            drug1buyten.onClick.AddListener(() => BuySellDrugs(1, 10));
            drug1sellone = GameObject.Find("Drug1SellOneButton").GetComponent<Button>();
            drug1sellone.onClick.AddListener(() => BuySellDrugs(1, -1));
            drug1sellten = GameObject.Find("Drug1SellTenButton").GetComponent<Button>();
            drug1sellten.onClick.AddListener(() => BuySellDrugs(1, -10));

            drug2invtxt = GameObject.Find("Drug2InvText").GetComponent<Text>();
            drug2buyone = GameObject.Find("Drug2BuyOneButton").GetComponent<Button>();
            drug2buyone.onClick.AddListener(() => BuySellDrugs(2, 1));
            drug2buyten = GameObject.Find("Drug2BuyTenButton").GetComponent<Button>();
            drug2buyten.onClick.AddListener(() => BuySellDrugs(2, 10));
            drug2sellone = GameObject.Find("Drug2SellOneButton").GetComponent<Button>();
            drug2sellone.onClick.AddListener(() => BuySellDrugs(2, -1));
            drug2sellten = GameObject.Find("Drug2SellTenButton").GetComponent<Button>();
            drug2sellten.onClick.AddListener(() => BuySellDrugs(2, -10));

            drug3invtxt = GameObject.Find("Drug3InvText").GetComponent<Text>();
            drug3buyone = GameObject.Find("Drug3BuyOneButton").GetComponent<Button>();
            drug3buyone.onClick.AddListener(() => BuySellDrugs(3, 1));
            drug3buyten = GameObject.Find("Drug3BuyTenButton").GetComponent<Button>();
            drug3buyten.onClick.AddListener(() => BuySellDrugs(3, 10));
            drug3sellone = GameObject.Find("Drug3SellOneButton").GetComponent<Button>();
            drug3sellone.onClick.AddListener(() => BuySellDrugs(3, -1));
            drug3sellten = GameObject.Find("Drug3SellTenButton").GetComponent<Button>();
            drug3sellten.onClick.AddListener(() => BuySellDrugs(3, -10));

            UpdateDrugs();
        }

        void UpdateDrugs()
        {
            drug1invtxt.text = string.Format("{0} @ ${1}", drug1count, drug1price);
            drug2invtxt.text = string.Format("{0} @ ${1}", drug2count, drug2price);
            drug3invtxt.text = string.Format("{0} @ ${1}", drug3count, drug3price);
        }

        void BuySellDrugs(int drug, int count)
        {
            if (drug == 1)
            {
                if ((InventoryScript.currency >= (drug1price * count)) && (drug1count >= count) && ((InventoryScript.drug1 + count) >= 0))
                {
                    InventoryScript.currency -= (drug1price * count);
                    InventoryScript.drug1 += count;
                    drug1count -= count;
                }
            }
            if (drug == 2)
            {
                if ((InventoryScript.currency >= (drug2price * count)) && (drug2count >= count) && ((InventoryScript.drug2 + count) >= 0))
                {
                    InventoryScript.currency -= (drug2price * count);
                    InventoryScript.drug2 += count;
                    drug2count -= count;
                }
            }
            if (drug == 3)
            {
                if ((InventoryScript.currency >= (drug3price * count)) && (drug3count >= count) && ((InventoryScript.drug3 + count) >= 0))
                {
                    InventoryScript.currency -= (drug3price * count);
                    InventoryScript.drug3 += count;
                    drug3count -= count;
                }
            }
        }

        void GetDrugPrice()
        {
            drug1price = (int)Random.Range(10.0f, 500.0f);
            drug2price = (int)Random.Range(10.0f, 500.0f);
            drug3price = (int)Random.Range(10.0f, 500.0f);

            drug1count = (int)Random.Range(0.0f, 999.0f);
            drug2count = (int)Random.Range(0.0f, 999.0f);
            drug3count = (int)Random.Range(0.0f, 999.0f);
            enteredshop = true;
        }

        void Update()
        {
            UpdateDrugs();
        }
    }
}