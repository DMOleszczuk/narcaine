﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace NarCaine
{
    public class PillScript : MonoBehaviour
    {
        Text poweruptxt;
        AudioSource powerupaudio;

        void Awake()
        {
            poweruptxt = GameObject.Find("PowerUpText").GetComponent<Text>();
            poweruptxt.enabled = false;
            powerupaudio = GameObject.Find("PowerUpAudio").GetComponent<AudioSource>();
        }

        void Update()
        {
            float rotspeed = 25.0f * Time.deltaTime;
            transform.Rotate(new Vector3(0.0f, 10.0f, 0.0f) * rotspeed, Space.World);
        }

        void OnTriggerEnter(Collider collider )
        {
            if(collider.GetComponent<Collider>().name == "player")
            {
                powerupaudio.Play();
                StartCoroutine(DisplayText());
            }
        }

        IEnumerator DisplayText()
        {
            poweruptxt.enabled = true;
            int countprize = (int)Random.Range(1.0f, 5.0f);
            //Start of same code as in BrothelScript.GetWinner()
            int drugnumprize = (int)Random.Range(1.0f, 3.9f);
            if (drugnumprize <= 1)
            {
                InventoryScript.drug1 += countprize;
                poweruptxt.text = string.Format("{0} {1}", countprize, InventoryScript.drug1name);
            }
            if (drugnumprize > 1 && drugnumprize <= 2)
            {
                InventoryScript.drug2 += countprize;
                poweruptxt.text = string.Format("{0} {1}", countprize, InventoryScript.drug2name);
            }
            if (drugnumprize > 2 && drugnumprize <= 3)
            {
                InventoryScript.drug3 += countprize;
                poweruptxt.text = string.Format("{0} {1}", countprize, InventoryScript.drug3name);
            }
            //end
            transform.Translate(new Vector3(0.0f, 100.0f, 0.0f), Space.World);
            yield return StartCoroutine(DeleteText());
        }

        IEnumerator DeleteText()
        {
            yield return new WaitForSeconds(2.0f);
            poweruptxt.enabled = false;
            Destroy(gameObject);
        }
    }
}