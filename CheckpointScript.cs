﻿using UnityEngine;
using System.Collections;

namespace NarCaine
{
    public class CheckpointScript : MonoBehaviour
    {
        GameObject blockade;

        public static bool crossed;
        void Awake()
        {
            crossed = false;
            blockade = GameObject.Find("Blockade");
            blockade.SetActive(false);
        }

        void OnTriggerEnter(Collider col)
        {
            if(col.name == "player")
            {
                crossed = true;
                blockade.SetActive(true);
            }
        }
    }
}