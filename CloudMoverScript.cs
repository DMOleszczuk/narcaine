﻿using UnityEngine;

public class CloudMoverScript : MonoBehaviour {

    Rigidbody FrontCloudRB;
    Rigidbody MiddleCloudRB;
    Rigidbody BackCloudRB;
    public float FrontSpeed;
    public float MiddleSpeed;
    public float BackSpeed;

	void Awake () {
        FrontCloudRB = GameObject.Find("frontbigcloud").GetComponent<Rigidbody>();
        MiddleCloudRB = GameObject.Find("middlebigcloud").GetComponent<Rigidbody>();
        BackCloudRB = GameObject.Find("backbigcloud").GetComponent<Rigidbody>();
	}
	
	void FixedUpdate () {
        FrontCloudRB.transform.position = new Vector3((FrontCloudRB.transform.position.x + FrontSpeed), FrontCloudRB.transform.position.y, FrontCloudRB.transform.position.z);
        MiddleCloudRB.transform.position = new Vector3((MiddleCloudRB.transform.position.x + MiddleSpeed), MiddleCloudRB.transform.position.y, MiddleCloudRB.transform.position.z);
        BackCloudRB.transform.position = new Vector3((BackCloudRB.transform.position.x + BackSpeed), BackCloudRB.transform.position.y, BackCloudRB.transform.position.z);
    }
}
