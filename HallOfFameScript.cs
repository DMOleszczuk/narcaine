﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using System.Collections.Generic;

namespace NarCaine
{
    public class HallOfFameScript : MonoBehaviour
    {
        static List<Text> place = new List<Text>();
        static List<int> tmplist = new List<int>();
        static int[] intlist = new int[5];
        int size = 5;
        int cur = InventoryScript.currency;
        public string filename;
        string filepath;

        void Awake()
        {
            filepath = string.Format("{0}/{1}", Application.persistentDataPath, filename);
            GetText();
            FileCreate();
            FileToList();
            Sort(cur);
            TmpToInt();
            DiplayScores();
            File.Delete(filepath);
            SaveToFile();
            ClearLists();
            BrothelScript.usedescortcenter = false;
            BrothelScript.usedescortleft = false;
            BrothelScript.usedescortright = false;
        }

        void GetText()
        {
            place.Add(GameObject.Find("OneText").GetComponent<Text>());
            place.Add(GameObject.Find("TwoText").GetComponent<Text>());
            place.Add(GameObject.Find("ThreeText").GetComponent<Text>());
            place.Add(GameObject.Find("FourText").GetComponent<Text>());
            place.Add(GameObject.Find("FiveText").GetComponent<Text>());
        }

        public void FileCreate()
        {
            if (!File.Exists(filepath))
            {
                FileStream stream = new FileStream(filepath, FileMode.OpenOrCreate);
                for (int i = 0; i < size; i++)
                {
                    stream.Write(System.Text.Encoding.UTF8.GetBytes("0"), 0, System.Text.Encoding.UTF8.GetByteCount("0"));
                    stream.Flush();
                    stream.Write(System.Text.Encoding.UTF8.GetBytes(Environment.NewLine), 0, System.Text.Encoding.UTF8.GetByteCount(Environment.NewLine));
                    stream.Flush();
                }
                stream.Close();
            }
            else
            {
                if (new FileInfo(filepath).Length <= 0)
                {
                    FileStream stream = new FileStream(filepath, FileMode.OpenOrCreate);
                    for (int i = 0; i < size; i++)
                    {
                        stream.Write(System.Text.Encoding.UTF8.GetBytes("0"), 0, System.Text.Encoding.UTF8.GetByteCount("0"));
                        stream.Flush();
                        stream.Write(System.Text.Encoding.UTF8.GetBytes(Environment.NewLine), 0, System.Text.Encoding.UTF8.GetByteCount(Environment.NewLine));
                        stream.Flush();
                    }
                    stream.Close();
                }
            }
        }

        public void FileToList()
        {
            string[] intxt = File.ReadAllLines(filepath);
            tmplist.Clear();
            //try catch here
            foreach (string str in intxt)
            {
                if (str != null)
                    tmplist.Add(Int32.Parse(str));
            }
        }

        public void exchange(int x, int y)
        {
            int tmp = tmplist[x];
            tmplist[x] = tmplist[y];
            tmplist[y] = tmp;
        }

        public void Sort(int number)
        {
            tmplist.Add(number);
            for (int i = tmplist.Count - 1; i > 0; i--)
                for (int j = 0; j < i; j++)
                    if (tmplist[j] > tmplist[j + 1])
                        exchange(j, j + 1);
        }

        public void TmpToInt()
        {
            for (int i = 0; i < size; i++)
                intlist[i] = tmplist[size - i];

            tmplist.Clear();
        }

        public void DiplayScores()
        {
            for (int i = 0; i < size; i++)
            {
                place[i].text = string.Format("{0}. {1}", i + 1, intlist[i]);
            }
        }

        public void SaveToFile()
        {
            FileStream stream = new FileStream(filepath, FileMode.OpenOrCreate);
            for (int i = 0; i < size; i++)
            {
                string snumb = intlist[i].ToString();
                stream.Write(System.Text.Encoding.UTF8.GetBytes(snumb), 0, System.Text.Encoding.UTF8.GetByteCount(snumb));
                stream.Flush();
                stream.Write(System.Text.Encoding.UTF8.GetBytes(Environment.NewLine), 0, System.Text.Encoding.UTF8.GetByteCount(Environment.NewLine));
                stream.Flush();
            }
            stream.Close();
        }

        void ClearLists()
        {
            place.Clear();
            tmplist.Clear();
        }

    }
}