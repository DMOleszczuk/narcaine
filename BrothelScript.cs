﻿using UnityEngine;
using UnityEngine.UI;

namespace NarCaine
{
    public class BrothelScript : MonoBehaviour
    {

        Canvas wincanvas;
        Text wintext;
        Text rightbuttontext;
        Text centerbuttontext;
        Text leftbuttontext;
        Button leftbutton;
        Button centerbutton;
        Button rightbutton;

        [SerializeField]
        int leftescort;
        [SerializeField]
        int centerescort;
        [SerializeField]
        int rightescort;

        public static bool usedescortleft = false;
        public static bool usedescortcenter = false;
        public static bool usedescortright = false;

        void Awake()
        {
            wincanvas = GameObject.Find("WinCanvas").GetComponent<Canvas>();
            wincanvas.enabled = false;

            wintext = GameObject.Find("WinText").GetComponent<Text>();

            leftbuttontext = GameObject.Find("LeftButtonText").GetComponent<Text>();
            leftbuttontext.text = string.Format("{0}{1}", "$", leftescort);

            centerbuttontext = GameObject.Find("CenterButtonText").GetComponent<Text>();
            centerbuttontext.text = string.Format("{0}{1}", "$", centerescort);

            rightbuttontext = GameObject.Find("RightButtonText").GetComponent<Text>();
            rightbuttontext.text = string.Format("{0}{1}", "$", rightescort);

            //check if able to buy escort
            //if not disable button
            int leftmoney = int.Parse(leftbuttontext.text.TrimStart('$'));
            leftbutton = GameObject.Find("LeftButton").GetComponent<Button>();
            if (InventoryScript.currency < leftmoney || usedescortleft == true)
            {
                leftbutton.interactable = false;
                leftbuttontext.enabled = false;
            }
            else
            {
                leftbutton.onClick.AddListener(() => GetWinner(GetOdds(leftmoney), "left"));
            }

            int centermoney = int.Parse(centerbuttontext.text.TrimStart('$'));
            centerbutton = GameObject.Find("CenterButton").GetComponent<Button>();
            if (InventoryScript.currency < centermoney || usedescortcenter == true)
            {
                centerbutton.interactable = false;
                centerbuttontext.enabled = false;
            }
            else
            {
                centerbutton.onClick.AddListener(() => GetWinner(GetOdds(centermoney), "center"));
            }

            int rightmoney = int.Parse(rightbuttontext.text.TrimStart('$'));
            rightbutton = GameObject.Find("RightButton").GetComponent<Button>();
            if (InventoryScript.currency < rightmoney || usedescortright == true)
            {
                rightbutton.interactable = false;
                rightbuttontext.enabled = false;
            }
            else
            {
                rightbutton.onClick.AddListener(() => GetWinner(GetOdds(rightmoney), "right"));
            }
        }

        float GetOdds(int escortprice)
        {
            return 10.0f - (escortprice/ 100.0f);
        }

        void GetWinner(float odds, string escort)
        {
            InventoryScript.currency = InventoryScript.currency - (1000 - ((int)odds * 100));
            float rand = Random.Range(1.0f, 10.0f);
            GameObject.Find("GirlCanvas").GetComponent<Canvas>().enabled = false;
            wincanvas.enabled = true;
            
            //win prize
            if (rand <= odds)
            {
                //number of drugs rewarded: flipped odds and multiplied
                int countprize = (int)Random.Range(1.0f * (10.0f - odds), 5.0f * ( 10.0f - odds));
                //drug name prize
                int drugnumprize = (int)Random.Range(1.0f, 3.9f);
                if(drugnumprize <= 1)
                {
                    InventoryScript.drug1 += countprize;
                    wintext.text = string.Format("{0} {1}", countprize, InventoryScript.drug1name);
                }
                if(drugnumprize > 1 && drugnumprize <=2)
                {
                    InventoryScript.drug2 += countprize;
                    wintext.text = string.Format("{0} {1}", countprize, InventoryScript.drug2name);
                }
                if(drugnumprize > 2 && drugnumprize <=3)
                {
                    InventoryScript.drug3 += countprize;
                    wintext.text = string.Format("{0} {1}", countprize, InventoryScript.drug3name);
                }
            }
            else
            {
                wintext.text = "Nothing!";
            }

            switch (escort)
            {
                case "left":
                    usedescortleft = true;
                    break;
                case "center":
                    usedescortcenter = true;
                    break;
                case "right":
                    usedescortright = true;
                    break;
                default:
                    break; 
            }
            
        }

        void Update()
        {



        }
    }
}