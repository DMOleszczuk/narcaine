﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

namespace NarCaine
{
    public class LoadLevelScript : MonoBehaviour {

        public RawImage image;
        public Text loadingtext;
        public Text progresstext;
        public Button[] buttons;
        public string[] levels;
        AsyncOperation async; 
        void Awake()
        {
            image.enabled = false;
            loadingtext.enabled = false;
            progresstext.enabled = false;
            async = null;

            for (int i = 0; i < buttons.Length; i++)
            {
                int j = i; //for the closures!
                buttons[j].onClick.AddListener(() => StartLoad(levels[j]));
            }
        }

        void StartLoad(string level)
        {
            image.enabled = true;
            loadingtext.enabled = true;
            progresstext.enabled = true;
            StartCoroutine(LoadLevel(level));
        }

        IEnumerator LoadLevel(string level)
        {
            async = SceneManager.LoadSceneAsync(level);
            while (!async.isDone)
            {
                progresstext.text = string.Format("{0}%", (int)(async.progress * 100));
                yield return null;
            }
            //yield return async;
        }
    }
}
