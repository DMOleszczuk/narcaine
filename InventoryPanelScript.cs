﻿using UnityEngine;
using UnityEngine.UI;

namespace NarCaine
{
    public class InventoryPanelScript : MonoBehaviour
    {
        Text casttxt;
        Text invtxt;
        
        void Awake()
        {
            casttxt = GameObject.Find("CashText").GetComponent<Text>();
            invtxt = GameObject.Find("InvText").GetComponent<Text>();
            UpdateInventory();
        }

        void UpdateInventory()
        {
            casttxt.text = string.Format("${0}", InventoryScript.currency.ToString());
            invtxt.text = string.Format("{0} {1} \n{2} {3} \n{4} {5}", NarCaine.InventoryScript.drug1.ToString(),
                NarCaine.InventoryScript.drug1name, NarCaine.InventoryScript.drug2.ToString(),
                NarCaine.InventoryScript.drug2name, NarCaine.InventoryScript.drug3.ToString(),
                NarCaine.InventoryScript.drug3name);
        }

        void Update()
        {
            UpdateInventory();
        }
    }
}