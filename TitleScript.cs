﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace NarCaine
{
    public class TitleScript : MonoBehaviour
    {

        public Button exitbutton;
        public Text credits;
        string[] creditstrg = { "Credits", "Programming and Graphics: David Oleszczuk", "Royalty Free Music: Bensound(www.bensound.com)" };

        void Awake()
        {
            InventoryScript.currency = 1000;
            InventoryScript.drug1 = 0;
            InventoryScript.drug2 = 0;
            InventoryScript.drug3 = 0;
            exitbutton.onClick.AddListener(() => ExitGame());
            StartCoroutine(CreditLoop());
        }

        void ExitGame()
        {
            Application.Quit();
        }

        IEnumerator CreditLoop()
        {
            for(int i = 0; i < creditstrg.Length+1; i++)
            {
                if(i >= creditstrg.Length)
                    i = 0;
               
                credits.text = creditstrg[i];
                yield return new WaitForSeconds(4.0f);
            }
            
        }
    }
}
