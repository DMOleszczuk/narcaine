﻿using UnityEngine;

namespace NarCaine
{
    public class EndTriggerScript : MonoBehaviour
    {
        Rigidbody playerrb;
        Canvas canvas;

        void Awake()
        {
            canvas = GameObject.Find("EndCanvas").GetComponent<Canvas>();
            canvas.enabled = false;
            playerrb = GameObject.Find("player").GetComponent<Rigidbody>();
        }

        void OnTriggerEnter(Collider col)
        {
            if (col.name == "player")
            {
                playerrb.constraints = RigidbodyConstraints.FreezeAll;
                PlayerScript.isstarted = false;
                canvas.enabled = true;
                InventoryScript.currency += (int)TimerScript.leveltime;
            }
        }
    }
}