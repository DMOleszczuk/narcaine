﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace NarCaine
{

    public class ClockScript : MonoBehaviour
    {
        Text poweruptxt;
        AudioSource powerupaudio;

        void Awake()
        {
            poweruptxt = GameObject.Find("PowerUpText").GetComponent<Text>();
            poweruptxt.enabled = false;
            powerupaudio = GameObject.Find("PowerUpAudio").GetComponent<AudioSource>();
        }

        void Update()
        {
            float rotspeed = 25.0f * Time.deltaTime;
            transform.Rotate(new Vector3(0.0f, 10.0f, 0.0f) * rotspeed, Space.World);
        }

        void OnTriggerEnter(Collider collider)
        {
            if (collider.GetComponent<Collider>().name == "player")
            {
                powerupaudio.Play();
                TimerScript.leveltime += 5.0f;
                StartCoroutine(DisplayText());
            }
        }

        IEnumerator DisplayText()
        {
            poweruptxt.enabled = true;
            poweruptxt.text = "+5 seconds!";
            transform.Translate(new Vector3(0.0f, 100.0f, 0.0f), Space.World);
            yield return StartCoroutine(DeleteText());
        }

        IEnumerator DeleteText()
        {
            yield return new WaitForSeconds(2.0f);
            poweruptxt.enabled = false;
            Destroy(gameObject);
        }
    }
}
