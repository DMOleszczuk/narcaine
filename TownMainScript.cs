﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

namespace NarCaine
{
    public class TownMainScript : MonoBehaviour
    {
        Button boatbutton;
        Text buttontext;
        public RawImage image;
        public Text text;

        void Awake()
        {
            image.enabled = false;
            text.enabled = false;
            boatbutton = GameObject.Find("BoatButton").GetComponent<Button>();
            boatbutton.onClick.AddListener(() => LoadBoat());
            buttontext = GameObject.Find("ButtonText").GetComponent<Text>();
            if(BoatLevelSelectScript.boatlevel == "hallOfFame")
            {
                buttontext.text = "Hall";
            }
            else
            {
                buttontext.text = "Boat";
            }
        }

        void LoadBoat()
        {
            image.enabled = true;
            text.enabled = true;
            StartCoroutine(EnterBoat());
            SetLevel();
        }
        
        void SetLevel()
        {
            switch(BoatLevelSelectScript.boatlevel)
            {
                case "boatLevelOne":
                    BoatLevelSelectScript.boatlevel = "boatLevelTwo";
                    break;
                case "boatLevelTwo":
                    BoatLevelSelectScript.boatlevel = "hallOfFame";
                    break;
                default:
                    BoatLevelSelectScript.boatlevel = "boatLevelOne";
                    break;
            }
        }

        IEnumerator EnterBoat()
        {
            AsyncOperation async = SceneManager.LoadSceneAsync(BoatLevelSelectScript.boatlevel);
            yield return async;
        }
    }
}