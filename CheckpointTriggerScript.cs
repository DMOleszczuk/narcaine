﻿using UnityEngine;
using System.Collections;

namespace NarCaine {
    public class CheckpointTriggerScript : MonoBehaviour {

        Transform playertf;
        Transform checkpoint1;
        Transform checkpoint2;
        
        void Awake() {
            playertf = GameObject.Find("player").GetComponent<Transform>();
            checkpoint1 = GameObject.Find("Checkpoint1").GetComponent<Transform>();
            checkpoint2 = GameObject.Find("Checkpoint2").GetComponent<Transform>();
        }

        void OnTriggerEnter(Collider col)
        {
            if (col.name == "player")
            {
                PlayerScript.playerrb.constraints = RigidbodyConstraints.FreezeAll;
                PlayerScript.playerrb.velocity = new Vector3(0.0f, 0.0f, 0.0f);

                PlayerScript.DisableWake();

                if (CheckpointScript.crossed)
                {
                    playertf.position = checkpoint2.position;
                    playertf.rotation = checkpoint2.rotation;
                }
                else
                {
                    playertf.position = checkpoint1.position;
                    playertf.rotation = checkpoint1.rotation;
                }
                PlayerScript.playerrb.constraints = RigidbodyConstraints.None;
                PlayerScript.leftwake.Clear();
                PlayerScript.rightwake.Clear();
                PlayerScript.centerwake.Clear();
            }
        }
    }
}