﻿using UnityEngine;
using UnityEngine.UI;

namespace NarCaine
{
    public class InventoryScript : MonoBehaviour
    {
        public static string drug1name = "Marijuana";
        public static string drug2name = "Molly";
        public static string drug3name = "Meth";

        private static int currencycount = 1000;
        public static int currency { get { return currencycount; } set { currencycount = value; } }

        private static int drug1count = 0;
        public static int drug1 { get { return drug1count; } set { drug1count = value; } }

        private static int drug2count = 0;
        public static int drug2 { get { return drug2count; } set { drug2count = value; } }

        private static int drug3count = 0;
        public static int drug3 { get { return drug3count; } set { drug3count = value; } }

        Text casttxt;
        Text invtxt;
    }
    
}
