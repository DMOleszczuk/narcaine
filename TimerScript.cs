﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace NarCaine
{
    public class TimerScript : MonoBehaviour
    {

        Text countdown;
        public static float leveltime;
        void Awake()
        {
            countdown = GameObject.Find("TimerText").GetComponent<Text>();
            if (SceneManager.GetActiveScene().name == "boatLevelOne")
                leveltime = 300.0f;
            if (SceneManager.GetActiveScene().name == "boatLevelTwo")
                leveltime = 350.0f;

            GetTime();
        }
        
        void GetTime()
        {
            countdown.text = string.Format("Timer: {0}", (int)leveltime);
        }

        void Update()
        {
            if(NarCaine.PlayerScript.isstarted == true)
            {
                leveltime -= Time.deltaTime;
                if(leveltime <= 0)
                {
                    leveltime = 0;
                }

                GetTime();
            }

        }
    }
}