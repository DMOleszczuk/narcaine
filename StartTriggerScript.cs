﻿using UnityEngine;

namespace NarCaine
{
    public class StartTriggerScript : MonoBehaviour
    {

        void OnTriggerExit(Collider col)
        {
            if (col.name == "player")
            {
                PlayerScript.isstarted = true;
                ShopScript.enteredshop = false;
                BrothelScript.usedescortcenter = false;
                BrothelScript.usedescortleft = false;
                BrothelScript.usedescortright = false;
            }
        }
    }
}