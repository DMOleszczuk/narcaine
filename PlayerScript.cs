﻿using UnityEngine;

namespace NarCaine
{

    public class PlayerScript : MonoBehaviour
    {
        public float movespeed;
        public float turnspeed;
        public float turndrag;
        public static bool isstarted;

        float tempdrag;

        ParticleSystem[] waterjets;

        public static Rigidbody playerrb;
        public static TrailRenderer rightwake;
        public static TrailRenderer leftwake;
        public static TrailRenderer centerwake;

        void Awake()
        {
            playerrb = GameObject.Find("player").GetComponent<Rigidbody>();
            playerrb.maxAngularVelocity = 0.0f;
            tempdrag = playerrb.drag;

            waterjets = GameObject.Find("WaterJets").GetComponentsInChildren<ParticleSystem>();

            rightwake = GameObject.Find("RightWake").GetComponent<TrailRenderer>();
            rightwake.enabled = false;
            leftwake = GameObject.Find("LeftWake").GetComponent<TrailRenderer>();
            leftwake.enabled = false;
            centerwake = GameObject.Find("CenterWake").GetComponent<TrailRenderer>();
            centerwake.enabled = false;

            isstarted = false;
        }

        public static void EnableWake()
        {
            rightwake.enabled = true;
            centerwake.enabled = true;
            leftwake.enabled = true;
        }

        public static void DisableWake()
        {
            rightwake.enabled = false;
            centerwake.enabled = false;
            leftwake.enabled = false;
        }

        void FixedUpdate()
        {
            if (Input.touchCount > 0)
            {
                RaycastHit2D hit = Physics2D.Raycast(Input.GetTouch(0).position, Vector2.zero);
                if(hit.collider != null)
                {
                    
                    if(hit.collider.name == "ForwardImage")
                    {
                        playerrb.drag = tempdrag;
                        playerrb.AddForce(transform.forward * movespeed);
                    }
                    if (hit.collider.name == "LeftImage")
                    {
                        playerrb.drag = turndrag;
                        playerrb.rotation = transform.localRotation * Quaternion.Euler(0.0f, -turnspeed, 0.0f);
                        playerrb.AddForce(transform.forward * (movespeed/2));
                    }
                    if (hit.collider.name == "RightImage")
                    {
                        playerrb.drag = turndrag;
                        playerrb.rotation = transform.localRotation * Quaternion.Euler(0.0f, turnspeed, 0.0f);
                        playerrb.AddForce(transform.forward * (movespeed/2));
                    }
                } 
            }

            if ((playerrb.velocity.z >= 0.1f) || (playerrb.velocity.z <= -0.1f))
            {
                foreach (ParticleSystem particles in waterjets)
                {
                    particles.Play();
                    EnableWake();
                }
            }
            else
            {
                foreach(ParticleSystem particles in waterjets)
                {
                    particles.Stop();
                    DisableWake();
                }
            }
      
        }
    }
}