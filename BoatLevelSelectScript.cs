﻿namespace NarCaine
{
    public class BoatLevelSelectScript
    {
        private static string boatlevelname = "boatLevelOne";
        public static string boatlevel { get { return boatlevelname; } set { boatlevelname = value; } }
    }
}